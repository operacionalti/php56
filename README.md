# php56 at Docker

Do you want to run php without actually install php on your machine?
How about run it inside a container? It's easy, I promess you!

After installing me you will simply run:

```bash
php56 some_script.php
```

Or, if you like to run the PHP built-in server:

```
php56 -S 0.0.0.0:80
```

Give it a try.

## Install

After you have [installed Docker](https://www.docker.com/community-edition#/download), just clone this repository and copy the `php56` shell script to your `/usr/local/bin` directory:

```bash
git clone git@gitlab.com:operacionalti/php56.git
sudo cp php56/php56 /usr/local/bin
sudo chmod +x /usr/local/bin/php56
```

That's all there is to it.
